#!/bin/bash
#
# Dot.tk DNS Updater
# by Kyle Kress
#
# Because Dot.tk domain cannot be updated easily via typical
# Dynamic DNS methods such as inadyn, this script will update
# the DNS automatically manually through my.dot.tk. The script
# would be good in a cron job or hooked to service that checked
# IP regularly. Dot.tk DNS Updater has been expanded from the
# original author to accept command line args, configuration
# files, CNAME record option, and multiple alias at once. Some
# of the urls and checks were updated as well.
#
# This script was modified from Benjamin Abendroth's script:
# http://www.beniweb.de/page/dottk.htm
#
# licensed under GPL3

git_url="https://bitbucket.org/toastal/dot.tk-dns-updater"

usage_text="Usage: `basename $0` [-c] [-f <config file>] [-u <username>] [-p <password>] [-a <domain alias>]+"

try_text=$(cat <<EOF
Try '`basename $0` -h' for help.
For more info see: `echo $git_url`
EOF
)

help_text=$(cat <<EOF
`echo $usage_text`

  -c    use CNAME record type for www.*.tk to *.tk
  -f    config file location
  -e    dot.tk email address (used as username)
  -p    dot.tk password
  -d    domain name with .tk (this option can be repeated to sync
        more than one site to the same IP address)
EOF
)

die() {
	echo "$2"
	rm -f "$cookie" "$result"
	exit $1
}

command_exists() {
    type "$1" &> /dev/null ;
}

addRecord() {
	# $1 = Type [ A | CNAME | MX ]
	# $2 = Hostname 
	# $3 = IP Address
	# $4 = ID on the fields (needs to be grep'd)

	POST+="&hosttype_$4=$1&hostname_$4=$2&hostcontent_$4=$3"
}

load_conf_file() {
	# $1 = file path

	if [[ $1 ]]; then
		if [[ -x $1 ]]; then
			. $1
			echo $domain_aliases

		else
			echo "Error: The config file given does not seem to exist."
			echo "Attempting to run anyhow..."
			echo
		fi
	fi
}

# If the default conf exists, load it
default_conf="/etc/tkupdater.conf"
if [[ -x $default_conf ]]; then
	. $default_conf
fi

# Parse command line arguments
while getopts "cf:a:e:p:h" opt; do
	case $opt in
		f)
			file_path=$OPTARG
			load_conf_file $file_path;;
		c)
			use_cname=true;;
		e)
			tk_email=$OPTARG;;
		p)
			tk_password=$OPTARG;;
		a)
			domain_aliases=(${domain_aliases[@]} $OPTARG);;
		h)
			echo "${help_text}"
			exit 1;;
		:|\?)
			echo $usage_text
			echo
			echo "${try_text}"
			exit 1;;
	esac
done

# Check to see all required variables are set
if [[ ! $domain_aliases ]] || [[ ! $tk_email ]] || [[ ! $tk_password ]]; then
	echo "Error: One or more of the variables are not set correctly. Check your command line arguments and config files and try again."
	echo
	echo $usage_text
	echo
	echo "${try_text}"
	exit 1
fi

echo "┌──────────────┐"
echo "  TKUpdater.sh"
echo "└──────────────┘"

# Make horizontal rules based on the length of the longest domain alias
longest_string=0
for domain in ${domain_aliases[@]}; do
	if [[ ${#domain} -gt $longest_string ]]; then
		longest_string=${#domain}
	fi
done
hr=$(printf '%*s' "$(($longest_string+2))" ' ' | sed "s/\s/─/g")

echo -n "[+] Getting external IP ... "
if command_exists "./getip.sh"; then
	current_ip=$(./getip.sh)
elif command_exists "dig"; then
	current_ip=$(dig myip.opendns.com @resolver1.opendns.com +short)
else
	current_ip=$(wget -qO- http://ipecho.net/plain)
fi

if [[ $? -eq 0 ]] && [[ -n "$current_ip" ]]; then
	echo "done. [$current_ip]"
else 
	die 1 "failed."
fi

agent='Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)'
cookie=`mktemp --suffix=dotTk`

echo -n "[+] Logging in ... "
wget -qO /dev/null -U "$agent" --no-check-certificate \
 --save-cookies "$cookie" --keep-session-cookies \
 --referer "http://my.dot.tk/cgi-bin/login01.taloha" \
 --post-data "fldemail=$tk_email&fldpassword=$tk_password" \
 "https://my.dot.tk/cgi-bin/login02.taloha"

if [[ $? -eq 0 ]]; then
	echo "done."
else
	die 1 "failed."
fi
echo

for alias in ${domain_aliases[@]}; do
	echo "┌$hr┐"
	echo "  $alias"
	echo "└$hr┘"
	
	# reset the POST var per alias
	POST=""

	echo -n "[+] Getting domain number ... "

	domain_number=$(wget -qO- -U "$agent" --no-check-certificate \
	 --load-cookies "$cookie" --keep-session-cookies \
	 "https://my.dot.tk/cgi-bin/domainpanel.taloha" | grep -i -B1 "$alias" \
	 | grep "domainnr" | grep -Eo '[0-9]+')

	if [[ $? -eq 0 ]] && [[ -n "$domain_number" ]]; then
		echo "done. [$domain_number]"
	else
		die 2 "failed."
	fi

	# Rip the form ids from the page to add to the POST data
	declare -a ids=($(wget -qO- -U "$agent" --no-check-certificate \
	 --load-cookies "$cookie" --keep-session-cookies \
	 "https://my.dot.tk/cgi-bin/domainpanel-modify.taloha?domainnr=$domain_number" \
	 | grep -Eo "hostcontent_[0-9]+" | grep -Eo "[0-9]+"))

	addRecord "A" "$alias" "$current_ip" `echo ${ids[0]}`
	if [[ $use_cname ]]; then
		addRecord "CNAME" "www.$alias" "$alias" `echo ${ids[1]}`
	else
		addRecord "A" "www.$alias" "$current_ip" `echo ${ids[1]}`
	fi

	echo -n "[+] Submitting form ... "
	result=`mktemp --suffix=dotTk.htm`
	wget -qO "$result" -U "$agent" --no-check-certificate \
	 --keep-session-cookies --load-cookies "$cookie" \
	 --referer "https://my.dot.tk/cgi-bin/domainpanel-modify.taloha?domainnr=$domain_number" \
	 --post-data "action=update&usage_type=dns_tk_dns&forward_url=http%3A%2F%2F&domainnr=$domain_number&domainname=$alias&nsname_1=&nscontent_1=$POST" \
	 "https://my.dot.tk/cgi-bin/domainpanel-modify.taloha"
	 
	if [[ $? -eq 0 ]] && grep -q "updated succesfully" "$result" ; then
		echo "Succesfully updated."
	else
		die 3 "failed."
	fi
	echo
	
	wget -qO "$result" -U "$agent" --no-check-certificate \
	 --keep-session-cookies --load-cookies "$cookie" \
	 "https://my.dot.tk/cgi-bin/logout.taloha"
done

echo "DNS Update Complete."
