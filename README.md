# Dot.tk DNS Updater
Because Dot.tk domain cannot be updated easily via typical Dynamic DNS methods such as inadyn, this script will update the DNS automatically manually through my.dot.tk. The script would be good in a cron job or hooked to service that checked IP regularly. Dot.tk DNS Updater has been expanded from the original author to accept command line args, configuration files, CNAME record option, and multiple aliases. Some of the urls and checks were updated as well.

## To Install 
Download the file into desired directory (I recommend `/usr/local/bin`)
#### Via `curl`
    curl -L https://bitbucket.org/toastal/dot.tk-dns-updater/raw/master/tkupdater.sh /usr/local/bin/tkupdater.sh
#### Via `wget`
    wget --no-check-certificate https://bitbucket.org/toastal/dot.tk-dns-updater/raw/master/tkupdater.sh -O /usr/local/bin/tkupdater.sh
#### Make the script executable
    chmod +x /usr/local/bin/tkupdater.sh

- - -

## Known Issues
`wget 1.14` Seems to currently have a [bug](http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=686837) with the arg `--no-check-certificate`. Currently I suggest downgrading until it is fixed.

- - -

## How To Use
#### Via command line args
    /usr/local/bin/tkupdater.sh -c -e email@address.com -p password -a example1.tk -a example2.tk -a example3.tk
#### Via conf file
	/usr/local/bin/tkupdater.sh -f /etc/tkupdater.conf
#### Via default conf file at `/etc/tkupdater.conf`
	/usr/local/bin/tkupdater.sh
The default location for a the `tkupdater.conf` file is at `/etc/tkupdater.conf`. You can make and modify your settings there to be automatically read without the `-f` command line arg or you can use the `-f` command line arg and include a path to a conf file elsewhere on the system. The main advantage to a conf file is an cleaner, easier to run command since the settings are saved and the process on the machine will not show you sensitive information (email and password). [More information on what the conf file should look like](https://bitbucket.org/toastal/dot.tk-dns-updater/src/master/example.conf).
